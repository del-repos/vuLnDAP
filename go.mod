module github.com/digininja/vuLnDAP

go 1.21.2

require (
	github.com/BurntSushi/toml v1.3.2
	github.com/nmcclain/ldap v0.0.0-20210720162743-7f8d1e44eeba
	github.com/sirupsen/logrus v1.9.3
	gopkg.in/ldap.v2 v2.5.1
	gopkg.in/russross/blackfriday.v2 v2.0.0
)

require (
	github.com/nmcclain/asn1-ber v0.0.0-20170104154839-2661553a0484 // indirect
	github.com/shurcooL/sanitized_anchor_name v1.0.0 // indirect
	golang.org/x/sys v0.0.0-20220715151400-c0bba94af5f8 // indirect
	gopkg.in/asn1-ber.v1 v1.0.0-20181015200546-f715ec2f112d // indirect
)
